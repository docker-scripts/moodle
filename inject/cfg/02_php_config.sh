#!/bin/bash -x

source /host/settings.sh

sed -i /etc/php/*/apache2/php.ini \
    -e "/^;max_input_vars =/ c max_input_vars = 5000"
sed -i /etc/php/*/cli/php.ini \
    -e "/^;max_input_vars =/ c max_input_vars = 5000"

if [[ -n $MAX_UPLOAD ]]; then
    sed -i /etc/php/*/apache2/php.ini \
        -e "/^post_max_size =/ c post_max_size = $MAX_UPLOAD" \
        -e "/^upload_max_filesize =/ c upload_max_filesize = $MAX_UPLOAD" \
        -e "/^max_execution_time =/ c max_execution_time = 60"
fi
