#!/bin/bash -x
### install moodle

# load global settings
global_settings=$(dirname $0)/global_settings.sh
[[ -f $global_settings ]] && source $global_settings

# load local settings
source /host/settings.sh

### create a directory for moodle data
mkdir -p /host/data
chown -R www-data /host/data
chmod -R 777 /host/data

### copy moodle code
if [[ ! -f /var/www/moodle/config.php ]]; then
    rm -rf /var/www/moodle
    cp -a /usr/local/src/moodle /var/www/
fi
git config --global --add safe.directory /var/www/moodle

### go to the moodle directory
cd /var/www/moodle/

### Get $MOODLE_BRANCH from git.
git pull
git checkout $MOODLE_BRANCH

### set some configuration defaults
if [[ -n $SMTP_SERVER ]]; then
    cat <<_EOF > local/defaults.php
<?php
\$defaults['moodle']['smtphosts'] = '$SMTP_SERVER';
\$defaults['moodle']['smtpsecure'] = 'TLS';
\$defaults['moodle']['smtpauthtype'] = 'PLAIN';
\$defaults['moodle']['smtpuser'] = '';
\$defaults['moodle']['smtppass'] = '';
_EOF
elif [[ -n $GMAIL_ADDRESS ]]; then
    cat <<_EOF > local/defaults.php
<?php
\$defaults['moodle']['smtphosts'] = 'smtp.gmail.com:465';
\$defaults['moodle']['smtpsecure'] = 'ssl';
\$defaults['moodle']['smtpauthtype'] = 'LOGIN';
\$defaults['moodle']['smtpuser'] = '$GMAIL_ADDRESS';
\$defaults['moodle']['smtppass'] = '$GMAIL_PASSWD';
_EOF
fi

### fix ownership
chown -R www-data: /var/www

### install moodle
if [[ -f /var/www/moodle/config.php ]]; then
    $php admin/cli/install_database.php \
        --agree-license \
        --lang="$SITE_LANG" --fullname="$SITE_FULLNAME" --shortname="$SITE_SHORTNAME" \
        --adminuser="$ADMIN_USER" --adminpass="$ADMIN_PASS" --adminemail="$ADMIN_EMAIL"
else
    $php admin/cli/install.php \
        --non-interactive --agree-license \
        --wwwroot="https://$DOMAIN" --dataroot="/host/data" \
        --dbtype="mariadb" --dbhost="${DBHOST:-localhost}" --dbport="${DBPORT:-3306}" \
        --dbname="${DBNAME:-moodle}" --dbuser="${DBUSER:-moodle}" --dbpass="${DBPASS:-moodle_pass}" \
        --lang="$SITE_LANG" --fullname="$SITE_FULLNAME" --shortname="$SITE_SHORTNAME" \
        --adminuser="$ADMIN_USER" --adminpass="$ADMIN_PASS" --adminemail="$ADMIN_EMAIL"
fi
