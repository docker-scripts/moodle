#!/bin/bash -x
### apache2 config

source /host/settings.sh

cat <<EOF > /etc/apache2/sites-available/$DOMAIN.conf
<VirtualHost *:80>
        ServerName $DOMAIN
        RedirectPermanent / https://$DOMAIN/
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $DOMAIN

        DocumentRoot /var/www/moodle
        <Directory /var/www/moodle/>
            AllowOverride All
        </Directory>

        SSLEngine on
        SSLCertificateFile	/etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

        <FilesMatch "\.(cgi|shtml|phtml|php)$">
            SSLOptions +StdEnvVars
        </FilesMatch>
</VirtualHost>
EOF

ip=$(hostname -I)
network=${ip%.*}.0/16
cat <<EOF > /etc/apache2/conf-available/remoteip.conf
RemoteIPTrustedProxy $network
RemoteIPHeader X-Forwarded-For
EOF

a2enmod ssl remoteip
a2enconf remoteip
a2ensite $DOMAIN
a2dissite 000-default
systemctl restart apache2
