#!/bin/bash -x
### create the database and user

source /host/settings.sh

mariadb -e "
    DROP DATABASE IF EXISTS moodle;
    CREATE DATABASE moodle;
    GRANT ALL ON moodle.* TO moodle@localhost IDENTIFIED BY 'moodle_pass';
"
