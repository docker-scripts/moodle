#!/bin/bash
### Upgrade moodle version, for example from 4.2 to 4.3

fail() { echo -n "$@" >&2 ; exit 1; }

source /host/settings.sh

### get version and the branch to upgrade to
### (something like 4.3 and MOODLE_403_STABLE)
usage="Usage: $0 <version> <branch>"
version=$1
[[ -n $version ]] || fail "$usage"
[[ $version == $MOODLE_VERSION ]] \
    || fail "MOODLE_VERSION on 'settings.sh' is different from '$version'."
branch=$2
[[ -n $branch ]] || fail "$usage"
[[ $branch == $MOODLE_BRANCH ]] \
    || fail "MOODLE_BRANCH on 'settings.sh' is different from '$branch'."

### go to the moodle directory
cd /var/www/moodle

### run cron, enable maintenance mode and purge caches
$php admin/cli/cron.php | grep 'task failed:'
$php admin/cli/maintenance.php --enable
$php admin/cli/purge_caches.php

### update moodle code with 'git pull'
sed -i ~/.gitconfig -e '/directory = \/var\/www\/moodle/ d'
git config --global --add safe.directory /var/www/moodle
git stash
git fetch
git checkout $branch
git pull
git stash pop

### update plugins
for file in $(git ls-files -o | grep version.php); do
    release=$(cat $file | grep '\->release' | cut -d= -f2 | tr -d " ';")
    [[ -z $release ]] && continue
    plugin=$(cat $file | grep '\->component' | cut -d';' -f1 | cut -d= -f2 | tr -d " ';")
    latest_version=$($moosh plugin-list -v | grep $plugin | tr , "\n" | sed '/https/d' | tail -1)
    support=$($moosh plugin-list | grep $plugin | tr , "\n" | sed '/https/d' | grep $version)
    if [[ -n $support ]]; then
        # install the latest version of the plugin
        $moosh plugin-install -d -f $plugin
    else
        echo "Plugin '$plugin' does not support yet the latest version of moodle ($release)."
        read -p "Keep it anyway? [y/N]: " ans
        ans=${ans:-n}
        ans=${ans,}
        if [[ $ans == 'y' ]]; then
            $moosh plugin-install -d -f -r $latest_version $plugin
        else
            rm -rf $(dirname $file)
            echo "Plugin directory $(dirname $file) was removed."
            echo "Try to add it manually later."
        fi
    fi
done

### fix ownership
chown www-data: -R .

### update the database
$php admin/cli/upgrade.php --non-interactive

### purge caches, disable maintenance mode and run cron
$php admin/cli/purge_caches.php
$php admin/cli/maintenance.php --disable
$php admin/cli/cron.php | grep 'task failed:'
