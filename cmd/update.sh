cmd_update_help() {
    cat <<_EOF
    update
        Update Moodle.

_EOF
}

cmd_update() {
    ds backup +data
    ds inject update.sh
}
