cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    # run standard config scripts
    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)

    # run config scripts
    ds inject cfg/01_mount_tmp_on_ram.sh
    ds inject cfg/02_php_config.sh
    ds inject cfg/03_apache2_config.sh
    if [[ -n $DBHOST ]]; then
        ds mariadb create
    else
        ds inject cfg/04_mariadb_config.sh
        ds inject cfg/05_create_db.sh
    fi
    ds inject cfg/06_moodle_install.sh
    ds inject cfg/07_moodle_config.sh
    ds inject cfg/08_setup_cron.sh
    ds inject cfg/09_setup_oauth2_google.sh

    # install additional plugins
    ds install-plugins

    # cleanup
    ds cc
}
