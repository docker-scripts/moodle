cmd_upgrade_help() {
    cat <<_EOF
    upgrade <version> <branch>
        Upgrade moodle version.
        <version> is a moodle version like 4.3
        <branch> is a git branch like MOODLE_403_STABLE

        Before calling this command, set the corresponding values
        to MOODLE_VERSION and MOODLE_BRANCH on 'settings.sh'.

_EOF
}

cmd_upgrade() {
    local version=$1
    [[ -n $version ]] || fail "Usage:\n$(cmd_upgrade_help)"
    [[ $version == $MOODLE_VERSION ]] \
        || fail "MOODLE_VERSION on 'settings.sh' is different from '$version'."
    local branch=$2
    [[ -n $branch ]] || fail "Usage:\n$(cmd_upgrade_help)"
    [[ $branch == $MOODLE_BRANCH ]] \
        || fail "MOODLE_BRANCH on 'settings.sh' is different from '$branch'."

    # make a backup
    ds backup +data
    ds inject upgrade.sh $version $branch
}
