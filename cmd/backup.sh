cmd_backup_help() {
    cat <<_EOF
    backup [+d | +data]
        Backup the Moodle database and config file.
        With option +d, the data directory is included in the backup as well.

_EOF
}

_php() {
    docker exec -u www-data -i $CONTAINER php "$@"
}

cmd_backup() {
    # get the option +data
    local data=0
    [[ $1 == '+d' || $1 == '+data' ]] && data=1

    # clear caches, enable maintenance mode, and stop the web server
    _php admin/cli/cron.php | grep 'task failed:'
    _php admin/cli/maintenance.php --enable
    _php admin/cli/purge_caches.php
    ds exec systemctl stop apache2

    # create a directory for collecting the backup data
    local datestamp=$(date +%F)
    local dir=backup-$CONTAINER-$datestamp
    rm -rf $dir/
    mkdir -p $dir/

    # dump the database
    local dump="mysqldump --allow-keywords --opt"
    [[ -n $DBHOST ]] \
	&& dump+=" -h $DBHOST -P $DBPORT -u $DBUSER --password=$DBPASS"
    ds exec $dump ${DBNAME:-moodle} > $dir/db.sql

    # copy the config file
    cp var-www/moodle/config.php $dir/

    # copy the data directory
    [[ $data == 1 ]] && cp -a data/ $dir/

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$dir.tgz $dir/

    # clean up
    rm -rf $dir/

    # start the web server and disable maintenance mode
    ds exec systemctl start apache2
    _php admin/cli/maintenance.php --disable
}
