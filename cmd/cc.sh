cmd_cc_help() {
    cat <<_EOF
    cc
        Clear the cache.

_EOF
}

cmd_cc() {
    local php="docker exec -u www-data -i $CONTAINER php"
    $php admin/cli/purge_caches.php

    ds exec sh -c "rm -rf /host/data/cache/*"
    ds exec sh -c "rm -rf /host/data/localcache/*"

    ds exec chown www-data: /host/data/cache/
    ds exec chown www-data: /host/data/localcache/
}
