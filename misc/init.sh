### run custom init steps

# customize settings.sh
source settings.sh
sed -i settings.sh \
    -e "/^DOMAIN=/ c DOMAIN='$CONTAINER'"

admin_pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w20 | head -n1)
sed -i settings.sh \
    -e "/^ADMIN_PASS=/ c ADMIN_PASS='$admin_pass'"
  
db_pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w20 | head -n1)
sed -i settings.sh \
    -e "s!DBNAME=.*!DBNAME='${CONTAINER//./_}'!" \
    -e "s!DBUSER=.*!DBUSER='${CONTAINER//./_}'!" \
    -e "s/DBPASS=.*/DBPASS='$db_pass'/"
  
# [[ -n $SMTP_SERVER ]] && \
#     sed -i settings.sh \
#        -e "/SMTP_HOST=/ c SMTP_HOST='$SMTP_SERVER'"
#
# [[ -n $SMTP_DOMAIN ]] && \
#     sed -i settings.sh \
#         -e "/ADMIN_EMAIL=/ c SMTP_FROM='moodle@$SMTP_DOMAIN'"
