moodle
======

Docker scripts that install and run Moodle in a container.

## Install

  - First install `ds` and `revproxy`:
     + https://gitlab.com/docker-scripts/ds#installation
     + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the moodle scripts: `ds pull moodle`

  - Create a directory for the moodle container: `ds init moodle @moodle1.example.org`

  - Fix the settings: `cd /var/ds/moodle1.example.org/; vim settings.sh`

  - Create the container and install Moodle: `ds make`

    *Note:* This will pull the image from DockerHub. To build the
    image yourself use `ds build` first, however this is usually
    slower.

## Other commands

```
ds shell
ds stop
ds start
ds help
```

## Backup and restore

```
ds backup
ds backup +data
ds restore backup-file.tgz
```

## Update and upgrade

To update the current stable branch (for example from 3.3.1 to 3.3.2)
use `ds update`. This is done quite frequently and usually has no
risks of breaking anything.

To upgrade to the next stable branch (for example from 4.2 to 4.3) use
`ds upgrade 4.3 MOODLE_403_STABLE`.

**Note:** Before giving this command, you will have to modify
accordingly `MOODLE_VERSION` and `MOODLE_BRANCH` on `settings.sh`.

**Note:** The upgrade command will try to upgrade the additional
plugins as well, if they have a version that matches the latest moodle
release. However this does not always work and some plugins may need
to be fixed manually.

Both update and upgrade make a backup before making any changes, just
in case.

## Remake

The command `ds remake` rebuilds everything from scratch, but
preserves the existing database and data files.

**Note:** After a `ds upgrade` it is recommended to edit
`settings.sh`, update the `IMAGE` variable, and do a `ds remake` as
well.
