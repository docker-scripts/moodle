APP=moodle
IMAGE=dockerscripts/moodle:405
MOODLE_BRANCH=MOODLE_405_STABLE
MOODLE_VERSION=4.5
#PORTS=

DOMAIN="moodle1.example.org"

### Uncomment DB settings to use an external MariaDB database.
#DBHOST=mariadb
#DBPORT=3306
#DBNAME=moodle1_example_org
#DBUSER=moodle1_example_org
#DBPASS=123456

### Moodle site settings.
SITE_LANG=en
SITE_FULLNAME="Moodle Example 1"
SITE_SHORTNAME="MDL1"

### Admin settings.
ADMIN_USER=admin
ADMIN_PASS="admin-1234"
ADMIN_EMAIL=admin@example.org

### Settings for register/login with OAuth2.
### See: https://developers.google.com/adwords/api/docs/guides/authentication#webapp
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=

### Additional plugins to be installed.
PLUGINS="
    atto_mathslate
    tinymce_mathslate
    theme_klass
    theme_moove
    theme_snap
    theme_stream
    theme_academi
"


### Settings for the plugin offlinequiz.
OFFLINEQUIZ_LOGOURL=

### Uncomment to set on PHP config:
###     post_max_size, upload_max_filesize, and max_execution_time
#MAX_UPLOAD=2G
