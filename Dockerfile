include(bookworm)

RUN <<EOF
  # update and upgrade and install some other packages
  apt update
  apt upgrade --yes
  apt install --yes \
      git wget curl unzip ca-certificates apt-transport-https

  # install mariadb 10.11.7
  curl -LO https://r.mariadb.com/downloads/mariadb_repo_setup
  chmod +x mariadb_repo_setup
  ./mariadb_repo_setup --mariadb-server-version="mariadb-10.11.7" --skip-maxscale
  rm mariadb_repo_setup
  apt install --yes \
      mariadb-server \
      mariadb-client \
      mariadb-backup \
      mariadb-plugin-provider-bzip2 \
      mariadb-plugin-provider-lz4 \
      mariadb-plugin-provider-lzma \
      mariadb-plugin-provider-lzo \
      mariadb-plugin-provider-snappy

  # install packages required by moodle
  DEBIAN_FRONTEND=noninteractive \
      apt install --yes \
          sudo apache2 graphviz aspell ghostscript clamav \
          php8.2 libapache2-mod-php8.2 php8.2-pspell php8.2-curl \
          php8.2-gd php8.2-intl php8.2-mysql php8.2-xml php8.2-xmlrpc \
          php8.2-ldap php8.2-zip php8.2-soap php8.2-mbstring

  # install moosh (http://moosh-online.com/)
  # https://moodle.org/plugins/view.php?id=522
  cd /usr/local/src/
  wget https://moodle.org/plugins/download.php/31885/moosh_moodle44_2024050100.zip
  unzip moosh_moodle44_2024050100.zip
  ln -s /usr/local/src/moosh/moosh.php /usr/local/bin/moosh
  rm moosh_moodle44_2024050100.zip
  #sed -i moosh/Moosh/MooshCommand.php -e '254 s/v/v ?? ""/'

  # get moodle code from git
  git clone --progress --verbose \
      https://github.com/moodle/moodle \
      /usr/local/src/moodle
EOF
